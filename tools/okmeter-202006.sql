insert into tools(name, title, version, version_date, url, has_oss_version, has_saas_version)
select 'okmeter-202006', 'OKmeter.io', '2020-06', '2020-06-20', 'https://okmeter.io', false, true;

\copy tool_metrics(tool_name, metric_name, vis_type, local_title, image_url, note) from './tools/okmeter-202006.csv' with delimiter ',' csv;
