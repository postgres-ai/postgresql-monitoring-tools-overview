# PostgreSQL Monitoring Tools Overview

## Initialization and data load

When database is empty, you can create schema and load all the data using this oneliner:

```bash
psql pgsql_mon < _initialize.sql
```

## Clean everything up

If you need to start from scratch, here is how you can delete everything (data, schema):

```bash
psql pgsql_mon < _destroy.sql
```
