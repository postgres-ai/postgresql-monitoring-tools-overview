create table public.metrics (
    name text primary key,
    created_at timestamptz not null default now(),
    title text,
    category text,
    subcategory text,
    extension text,
    importance text,
    description text
);

alter table public.metrics owner to mon;

create table public.tools (
    name text not null primary key,
    created_at timestamptz not null default now(),
    title text not null,
    version text not null,
    version_date date,
    url text not null,
    has_oss_version bool,
    has_saas_version bool,
    note text
);

alter table public.tools owner to mon;


create table public.tool_metrics (
    tool_name text not null references tools (name),
    metric_name text not null references metrics (name),
    vis_type text not null check (vis_type in ('graph', 'table')),
    created_at timestamp with time zone default now(),
    local_title text,
    image_url text,
    note text,
    primary key (tool_name, metric_name, vis_type)
);

alter table public.tool_metrics owner to mon;
