insert into metrics(name, title, category, subcategory, extension, importance, description)
values
  ('pgss_topn_total_time', 'PGSS: Top-N by total_time', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_calls', 'PGSS: Top-N by calls', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_mean_time', 'PGSS: Top-N by mean_time', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_blk_read_time', 'PGSS: Top-N by blk_read_time', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_blk_write_time', 'PGSS: Top-N by blk_write_time', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_shared_blks_read', 'PGSS: Top-N by shared_blks_read', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_shared_blks_hit', 'PGSS: Top-N by shared_blks_hit', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_rows', 'PGSS: Top-N by rows', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_temp_blks_read', 'PGSS: Top-N by temp_blks_read', 'pg_stat_statements', 'Top-N', null, null, null),
  ('pgss_topn_temp_blks_hit', 'PGSS: Top-N by temp_blks_hit', 'pg_stat_statements', 'Top-N', null, null, null)
;
